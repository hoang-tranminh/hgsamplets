﻿using AngularJS.WebApi.Models;
using System.Collections;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace AngularJS.WebApi.Business
{
    public interface ITimeEntryManager
    {
        Task<bool> AddTimeEntry(TimeEntry entry);

        Task<bool> SaveTimeEntry(TimeEntry entry);

        Task<bool> DeleteTimeEntry(int id);

        Task<IEnumerable<TimeEntry>> GetTimeEntries(int weekNumber);

        Task<IEnumerable<Project>> GetProjects();
    }
}