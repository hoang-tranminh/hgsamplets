﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Data.Entity;
using System.Web;
using AngularJS.WebApi.Models;
using AngularJS.WebApi.DAL;
using System.Globalization;
using System.Threading.Tasks;

namespace AngularJS.WebApi.Business
{
    public class TimeEntryManager : ITimeEntryManager
    {

        private static void InitiateTestData()
        {
            
        }

        public async Task<bool> AddTimeEntry(TimeEntry entry)
        {
            try
            {
                var context = new TimesheetDBContext();
                context.TimeEntry.Add(entry);
                await context.SaveChangesAsync();
                return true;
            }
            catch (Exception ex)
            {
                return false;
            }
        }

        public async Task<bool> DeleteTimeEntry(int id)
        {
            try
            {
                var context = new TimesheetDBContext();
                var en =context.TimeEntry.First(e => e.Id == id);
                context.TimeEntry.Remove(en);
                await context.SaveChangesAsync();
                return true;
            }
            catch (Exception ex)
            {
                return false;
            }
        }

        public async Task<IEnumerable<TimeEntry>> GetTimeEntries(int weekNumber)
        {
            try
            {
                var startDate = Utils.FirstDateOfWeekISO8601(DateTime.UtcNow.Year, weekNumber);
                var endDate = startDate.AddDays(7).AddSeconds(-1);
                var context = new TimesheetDBContext();
                return await context.TimeEntry.AsNoTracking().Where(e => e.Date >= startDate && e.Date <= endDate).ToListAsync();
            }
            catch (Exception ex)
            {
                return new List<TimeEntry>();
            }
        }

        public async Task<IEnumerable<Project>> GetProjects()
        {
            try
            {
                var context = new TimesheetDBContext();
                return await context.Project.ToListAsync();
            }
            catch (Exception ex)
            {
                return new List<Project>();
            }
        }

        public async Task<bool> SaveTimeEntry(TimeEntry entry)
        {
            try
            {
                var context = new TimesheetDBContext();
                var en = context.TimeEntry.First(e => e.Id == entry.Id);
                en.Notes = entry.Notes;
                en.ProjectId = entry.ProjectId;
                en.TaskId = entry.TaskId;
                await context.SaveChangesAsync();
                return true;
            }
            catch (Exception ex)
            {
                return false;
            }
        }

      
    }
}