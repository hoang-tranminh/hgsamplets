﻿using AngularJS.WebApi.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace AngularJS.WebApi.DAL
{
    public class DbInitializer : System.Data.Entity.DropCreateDatabaseAlways<TimesheetDBContext>
    {
        protected override void Seed(TimesheetDBContext context)
        {
            var projects = new List<Project>() {
                new Project() { Name="Project A" },
                new Project() { Name="Project B" },
            };
            projects.ForEach(p => {
                p= context.Project.Add(p);
            });
            context.SaveChanges();

            var tasks = new List<ProjectTask>();
            foreach (var pId in projects)
            {
                for (int i = 0; i < 3; i++)
                {
                    var t = new ProjectTask() { Name = "Task " + pId.Name + "-" + i.ToString(), ProjectId = pId.Id };
                    context.ProjectTask.Add(t);
                    tasks.Add(t);
                }
                context.SaveChanges();
            }

            List<TimeEntry> entries = new List<TimeEntry>();

            //today
            for (int i = 0; i < 3; i++)
            {
                entries.Add(new TimeEntry { Date= DateTime.Now.Date, Hours= DateTime.Now.Date.Day % 12 + i, Minutes=40+i, ProjectId= projects.ElementAt(0).Id, TaskId= tasks.Where(t => t.ProjectId == projects.ElementAt(0).Id).ElementAt(i).Id });
            }
            for (int i = 0; i < 3; i++)
            {
                entries.Add(new TimeEntry { Date = DateTime.Now.Date, Hours = DateTime.Now.Date.Day % 12 + i, Minutes = 40 + i, ProjectId = projects.ElementAt(1).Id, TaskId = tasks.Where(t => t.ProjectId == projects.ElementAt(1).Id).ElementAt(i).Id });
            }

            //yesterday
            for (int i = 0; i < 3; i++)
            {
                entries.Add(new TimeEntry { Date = DateTime.Now.Date.AddDays(-1), Hours = DateTime.Now.Date.AddDays(-1).Day % 12 + i, Minutes = 40 + i, ProjectId = projects.ElementAt(0).Id, TaskId = tasks.Where(t => t.ProjectId == projects.ElementAt(0).Id).ElementAt(i).Id });
            }
            for (int i = 0; i < 3; i++)
            {
                entries.Add(new TimeEntry { Date = DateTime.Now.Date.AddDays(-1), Hours = DateTime.Now.Date.AddDays(-1).Day % 12 + i, Minutes = 40 + i, ProjectId = projects.ElementAt(1).Id, TaskId = tasks.Where(t => t.ProjectId == projects.ElementAt(1).Id).ElementAt(i).Id });
            }

            //tomorrow
            for (int i = 0; i < 3; i++)
            {
                entries.Add(new TimeEntry { Date = DateTime.Now.Date.AddDays(1), Hours = DateTime.Now.Date.AddDays(1).Day % 12 + i, Minutes = 40 + i, ProjectId = projects.ElementAt(0).Id, TaskId = tasks.Where(t => t.ProjectId == projects.ElementAt(0).Id).ElementAt(i).Id });
            }
            for (int i = 0; i < 3; i++)
            {
                entries.Add(new TimeEntry { Date = DateTime.Now.Date.AddDays(1), Hours = DateTime.Now.Date.AddDays(1).Day % 12 + i, Minutes = 40 + i, ProjectId = projects.ElementAt(1).Id, TaskId = tasks.Where(t => t.ProjectId == projects.ElementAt(1).Id).ElementAt(i).Id });
            }


            //next week
            for (int i = 0; i < 3; i++)
            {
                entries.Add(new TimeEntry { Date = DateTime.Now.Date.AddDays(7), Hours = DateTime.Now.Date.AddDays(7).Day % 12 + i, Minutes = 40 + i, ProjectId = projects.ElementAt(0).Id, TaskId = tasks.Where(t => t.ProjectId == projects.ElementAt(0).Id).ElementAt(i).Id });
            }
            for (int i = 0; i < 3; i++)
            {
                entries.Add(new TimeEntry { Date = DateTime.Now.Date.AddDays(7), Hours = DateTime.Now.Date.AddDays(7).Day % 12 + i, Minutes = 40 + i, ProjectId = projects.ElementAt(1).Id, TaskId = tasks.Where(t => t.ProjectId == projects.ElementAt(1).Id).ElementAt(i).Id });
            }

            //previous week
            for (int i = 0; i < 3; i++)
            {
                entries.Add(new TimeEntry { Date = DateTime.Now.Date.AddDays(-7), Hours = DateTime.Now.Date.AddDays(-7).Day % 12 + i, Minutes = 40 + i, ProjectId = projects.ElementAt(0).Id, TaskId = tasks.Where(t => t.ProjectId == projects.ElementAt(0).Id).ElementAt(i).Id });
            }
            for (int i = 0; i < 3; i++)
            {
                entries.Add(new TimeEntry { Date = DateTime.Now.Date.AddDays(-7), Hours = DateTime.Now.Date.AddDays(-7).Day % 12 + i, Minutes = 40 + i, ProjectId = projects.ElementAt(1).Id, TaskId = tasks.Where(t => t.ProjectId == projects.ElementAt(1).Id).ElementAt(i).Id });
            }


            context.TimeEntry.AddRange(entries);
            context.SaveChanges();

            base.Seed(context);
        }
    }
}