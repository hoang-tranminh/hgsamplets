﻿using AngularJS.WebApi.Models;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Data.Entity.ModelConfiguration.Conventions;
using System.Linq;
using System.Web;

namespace AngularJS.WebApi.DAL
{
    public class TimesheetDBContext: DbContext
    {
        public TimesheetDBContext() : base("DefaultConnection")
        {
            Database.SetInitializer<TimesheetDBContext>(new DbInitializer());

        }

        public DbSet<TimeEntry> TimeEntry { get; set; }
        public DbSet<Project> Project { get; set; }
        public DbSet<ProjectTask> ProjectTask { get; set; }

        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            modelBuilder.Conventions.Remove<PluralizingTableNameConvention>();
            modelBuilder.Conventions.Remove<OneToManyCascadeDeleteConvention>();
        }

        
    }
}