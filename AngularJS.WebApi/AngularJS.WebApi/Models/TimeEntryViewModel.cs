﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace AngularJS.WebApi.Models
{
    public class TimeEntryViewModel:TimeEntry
    {
        public string ProjectName { get; set; }

        public string TaskName { get; set; }
    }
}