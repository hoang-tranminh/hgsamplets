﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace AngularJS.WebApi.Models
{
    public class Project
    {
        public int Id { get; set; }

        public string Name { get; set; }

        public List<ProjectTask> Tasks { get; set; }
    }
}