﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace AngularJS.WebApi.Models
{
    public class ProjectTask
    {
        public int Id { get; set; }

        public int ProjectId { get; set; }

        public virtual Project Project { get; set; }

        public string Name { get; set; }

    }
}