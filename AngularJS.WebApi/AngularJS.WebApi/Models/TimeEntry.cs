﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace AngularJS.WebApi.Models
{
    public class TimeEntry
    {
        public int Id { get; set; }

        public DateTime Date { get; set; }

        public int ProjectId { get; set; }

        public int TaskId { get; set; }

        public int Hours { get; set; }

        public int Minutes { get; set; }

        public virtual Project Project { get; set; }

        public virtual ProjectTask Task { get; set; }

        public string Notes { get; set; }
    }
}