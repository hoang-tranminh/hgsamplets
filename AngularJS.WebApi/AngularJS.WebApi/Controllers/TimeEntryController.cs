﻿using AngularJS.WebApi.Business;
using AngularJS.WebApi.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Threading.Tasks;
using System.Web.Http;

namespace AngularJS.WebApi.Controllers
{
    [RoutePrefix("api/timeentry")]
    public class TimeEntryController : ApiController
    {
        private ITimeEntryManager _manager;
        public TimeEntryController()
        {
            _manager = new TimeEntryManager();
        }

        //GET timeentry/entries
        [Route("Entries/{weekNumber:int}")]
        public async Task<IEnumerable<TimeEntryViewModel>> GetEntries(int weekNumber)
        {
            var entries = await _manager.GetTimeEntries(weekNumber);
            return entries.Select(e => new TimeEntryViewModel() {
                Date = e.Date,
                Id = e.Id,
                Notes = e.Notes,
                ProjectId = e.ProjectId,
                ProjectName = e.Project.Name,
                TaskName = e.Task.Name,
                TaskId = e.TaskId,
                Hours = e.Hours,
                Minutes = e.Minutes
            });
        }

        //POST timeentry/entry
        [Route("AddEntry")]
        public async Task<bool> AddEntry(TimeEntry entry)
        {
            return await _manager.AddTimeEntry(entry);
        }

        //POST timeentry/entry
        [Route("SaveEntry")]
        public async Task<bool> SaveEntry(TimeEntry entry)
        {
            return await _manager.SaveTimeEntry(entry);
        }

        //POST timeentry/entry
        [Route("DeleteEntry")]
        public async Task<bool> DeleteEntry(int id)
        {
            return await _manager.DeleteTimeEntry(id);
        }
    }
}
