'use strict';

angular.module('timeTrackingDashboard', ['ngRoute','sharedModule'])

.config(['$routeProvider', function($routeProvider) {
  $routeProvider.when('/timetracking/dashboard', {
    templateUrl: 'time-tracking/dashboard/time-tracking-dashboard.template.html',
    controller: 'timeTrackingDashboardCtrl'
  });
}]);