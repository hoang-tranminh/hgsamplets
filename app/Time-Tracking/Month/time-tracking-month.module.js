'use strict';

angular.module('timeTrackingMonth', ['ngRoute','sharedModule'])

.config(['$routeProvider', function($routeProvider) {
  $routeProvider.when('/timetracking/month', {
    templateUrl: 'time-tracking/month/time-tracking-month.template.html',
    controller: 'timeTrackingMonthCtrl'
  });
}]);