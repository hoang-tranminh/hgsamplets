angular.
  module('timeTrackingWeek').
  component('taskList', {
    templateUrl: '/time-tracking/week/task-list/task-list.template.html',
    controller: ['$scope', '$routeParams','entryService','utils',
      function TaskListController($scope, $routeParams, entryService, utils) {
            var self= this;

            self.selectedDate  =  $scope.$parent.selectedDayRoot;

            var listener = $scope.$on('selectedDayChanged', function (event, data) {
              self.selectedDate  = data;
            });

            self.$onDestroy = function(){
               listener();//unsubcribe!;
            };

            self.addClass= function(entry) {
                var css = 'taskRow';
                var ed = new Date(entry.Date);
                if( ed.getDate() != self.selectedDate.getDate()) {
                  css += " hidden";
                }
                return css;
            };

            if(!$routeParams.weekNumber)
              var week = utils.ISO8601_week_no(new Date());
            else
              week = parseInt($routeParams.weekNumber);
            
            self.entries = entryService.query({weekNumber:week}, function(){
              //do something here!

            });
      }
    ]
  });