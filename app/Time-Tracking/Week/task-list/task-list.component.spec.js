'use strict';

describe('timeTrackingWeek', function () {

    // Load the module that contains the `phoneList` component before each test
    beforeEach(module('timeTrackingWeek'));

    // Test the controller
    describe('TaskListController', function () {
        var $httpBackend, ctrl, utils, scope;
        var entryData = [
            {
                "ProjectName": "Project A",
                "TaskName": "Task Project A-0",
                "Id": 1,
                "Date": "2016-11-16T00:00:00",
                "ProjectId": 1,
                "TaskId": 1,
                "Hours": 4,
                "Minutes": 40,
                "Project": null,
                "Task": null,
                "Notes": null
            }
        ];


        beforeEach(inject(function ($rootScope, $componentController, _$httpBackend_, _utils_) {
            scope = $rootScope.$new();

            //Hard selectedDayRoot
            scope.$parent.selectedDayRoot = new Date();
            $httpBackend = _$httpBackend_;
            utils = _utils_;
            var weekNumber = utils.ISO8601_week_no(new Date());
            $httpBackend.expectGET('/api/timeentry/Entries/' + weekNumber)
                                    .respond(200,entryData);
            ctrl = $componentController('taskList', { $scope: scope }, {});
            
        }));

        it("should addClass in component 'taskList' with entry fetched with '$http'",
            function() {
                jasmine.addCustomEqualityTester(angular.equals);
                expect(ctrl.entries).toEqual([]);
                $httpBackend.flush();
                expect(ctrl.entries.length).toEqual(1);
               // debugger;
                var entryItem = ctrl.entries[0];
                var cssClass = ctrl.addClass(entryItem);
                expect(cssClass).toBe("taskRow hidden");
            });
    });
});
