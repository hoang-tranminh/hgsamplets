'use strict';

angular.module('timeTrackingWeek', ['ngRoute','sharedModule','services'])

.config(['$routeProvider', function($routeProvider) {
  $routeProvider.when('/timetracking/week/:weekNumber', {
    templateUrl: 'time-tracking/week/time-tracking-week.template.html',
    controller: 'timeTrackingWeekCtrl'
  });
  $routeProvider.when('/timetracking/week/', {
    templateUrl: 'time-tracking/week/time-tracking-week.template.html',
    controller: 'timeTrackingWeekCtrl',
  });
}]);