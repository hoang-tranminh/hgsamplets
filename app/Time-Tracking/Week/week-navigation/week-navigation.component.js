angular.
  module('timeTrackingWeek').
  component('weekNavigation', {
    templateUrl: '/time-tracking/week/week-navigation/week-navigation.template.html',
    controller: ['$routeParams','utils',
      function WeekNavigationController($routeParams, utils) {
            var self= this;

            if(!$routeParams.weekNumber)
              self.currentWeek = utils.ISO8601_week_no(new Date());
            else
              self.currentWeek = parseInt($routeParams.weekNumber);
            self.currentWeekStr = "Week " + self.currentWeek;

            self.nextWeekLink = "#!/timetracking/week/" + (self.currentWeek +1);
            self.preWeekLink = "#!/timetracking/week/" + (self.currentWeek -1);
            
      }
    ]
  });