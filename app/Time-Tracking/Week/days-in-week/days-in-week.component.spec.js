'use strict';

describe('timeTrackingWeek', function () {

    // Load the module that contains the `phoneList` component before each test
    beforeEach(module('timeTrackingWeek'));

    // Test the controller
    describe('DaysInWeekController', function () {
        var ctrl, utils, scope, routeParams;

        beforeEach(inject(function ($rootScope, $componentController, $routeParams, _utils_) {
            scope = $rootScope.$new();
            utils = _utils_;
            //Hard selectedDayRoot
            scope.$parent.selectedDayRoot = new Date();
            routeParams = $routeParams;
            routeParams.weekNumber= utils.ISO8601_week_no(new Date());
            ctrl = $componentController('daysInWeek', { $scope: scope, $routeParams: routeParams }, {});
          
        }));

        it("should selectedDateStr in component 'daysInWeek'",
            function() {
                jasmine.addCustomEqualityTester(angular.equals);

                var options = {
                    weekday: "long", month: "short",
                    day: "numeric"
                };
                var dayNumberExpect = utils.getDateOfISOWeek(parseInt(routeParams.weekNumber), 2016);
                var selectedDateStrExpect = dayNumberExpect.toLocaleTimeString("en-us", options);
                expect(ctrl.selectedDateStr).toBe(selectedDateStrExpect);

                //  expect(ctrlTemp.selectedDateStr).toEqual(selectedDateStrExpect);
              //debugger;
            });


        it("should selectedClass in component 'daysInWeek'",
           function () {
               jasmine.addCustomEqualityTester(angular.equals);

               var options = {
                   weekday: "long", month: "short",
                   day: "numeric"
               };
               var dayNumberExpect = utils.getDateOfISOWeek(parseInt(routeParams.weekNumber), 2016);
               expect(ctrl.selectedClass(dayNumberExpect.getDay())).toBe("selected");
            
           });

    });
});
