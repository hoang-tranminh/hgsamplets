angular.
  module('timeTrackingWeek').
  component('daysInWeek', {
    templateUrl: '/time-tracking/week/days-in-week/days-in-week.template.html',
    controller: ['$scope','$routeParams','utils',
      function DaysInWeekController($scope, $routeParams, utils) {
            var self= this;
            
            if(!$routeParams.weekNumber)
              self.selectedDate = new Date();
            else
              self.selectedDate = utils.getDateOfISOWeek(parseInt($routeParams.weekNumber), 2016);
            
            
            $scope.$parent.selectedDayRoot = self.selectedDate;
            

            var options = {
              weekday: "long", month: "short",
              day: "numeric"
            };


            self.selectedDateStr =  self.selectedDate.toLocaleTimeString("en-us", options);
            
            self.selectedClass = function(d) {
                return self.selectedDate.getDay() == d ? "selected" : "";
            };

            self.select = function(d) {
                self.selectedDate = self.addDays(self.selectedDate, d - self.selectedDate.getDay());
                
                self.selectedDateStr =  self.selectedDate.toLocaleTimeString("en-us", options);
            
                $scope.$parent.$broadcast('selectedDayChanged', self.selectedDate ); // going up!
            };

            self.addDays = function (date, days) {
                var result = new Date(date);
                result.setDate(result.getDate() + days);
                return result;
            };
      }
    ]
  });