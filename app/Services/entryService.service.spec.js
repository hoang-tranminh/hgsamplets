'use strict';

describe('entryService', function () {
    var $httpBackend;
    var utils;
    var entryService;
    var entriesData = [
     {
         "ProjectName": "Project A",
         "TaskName": "Task Project A-0",
         "Id": 1,
         "Date": "2016-11-16T00:00:00",
         "ProjectId": 1,
         "TaskId": 1,
         "Hours": 4,
         "Minutes": 40,
         "Project": null,
         "Task": null,
         "Notes": null
     }
    ];
    //var entriesData = [];
    // Add a custom equality tester before each test
    beforeEach(function () {
        jasmine.addCustomEqualityTester(angular.equals);
    });

    // Load the module that contains the `entryService` service before each test
    beforeEach(module('services'));

    // Instantiate the service and "train" `$httpBackend` before each test
    beforeEach(inject(function (_$httpBackend_, _entryService_, _utils_) {
        $httpBackend = _$httpBackend_;
        utils = _utils_;
        var weekNumber = utils.ISO8601_week_no(new Date());
        // var weekNumber = 47;

        $httpBackend.expectGET('/api/timeentry/Entries/' + weekNumber).respond(entriesData);
        entryService = _entryService_;
    }));

    // Verify that there are no outstanding expectations or requests after each test
    afterEach(function () {
        $httpBackend.verifyNoOutstandingExpectation();
        $httpBackend.verifyNoOutstandingRequest();
    });

    it('should fetch the entries data from `/api/timeentry/Entries/:weekNumber`', function () {
        var entries = entryService.query();
        expect(entries).toEqual([]);
        $httpBackend.flush();
        expect(entries).toEqual(entriesData);
    });

});
