angular.module('services').factory('entryService',['$resource','utils',
        function($resource, utils){
            return $resource('http://localhost:35368/api/timeentry/Entries/:weekNumber',{},{
                query: {
                    method: 'GET',
                    params: {weekNumber: utils.ISO8601_week_no(new Date()) },
                    isArray:true
                }
            });
}]);