angular.module('services').factory('utils', function(){
            return { 
                    ISO8601_week_no : function (dt) {  
                        var tdt = new Date(dt.valueOf());  
                        var dayn = (dt.getDay() + 6) % 7;  
                        tdt.setDate(tdt.getDate() - dayn + 3);  
                        var firstThursday = tdt.valueOf();  
                        tdt.setMonth(0, 1);  
                        if (tdt.getDay() !== 4)   
                        {  
                            tdt.setMonth(0, 1 + ((4 - tdt.getDay()) + 7) % 7);  
                        }  
                        return 1 + Math.ceil((firstThursday - tdt) / 604800000);  
                    },
                    getDateOfISOWeek: function(w, y) {
                        var simple = new Date(y, 0, 1 + (w - 1) * 7);
                        var dow = simple.getDay();
                        var ISOweekStart = simple;
                        if (dow <= 4)
                            ISOweekStart.setDate(simple.getDate() - simple.getDay() + 1);
                        else
                            ISOweekStart.setDate(simple.getDate() + 8 - simple.getDay());
                        return ISOweekStart;
                    }
                }
});