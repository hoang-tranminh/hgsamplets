'use strict';

// Declare app level module which depends on views, and components
angular.module('HgSampleTs', [
  'ngRoute',
  'timeTracking',
  'sharedModule',
  'timeTrackingWeek',
   'timeTrackingMonth',
    'timeTrackingDashboard',
    'services'
  /*,
  'myApp.view1',
  'myApp.view2',
  'myApp.version'*/
]).
config(['$locationProvider', '$routeProvider', function($locationProvider, $routeProvider) {
  $locationProvider.hashPrefix('!');

  $routeProvider.otherwise({redirectTo: '/timetracking/week'});
}]);
